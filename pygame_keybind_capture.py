'''
Script to capture mouse and keybind
'''

import pygame

'''
Intializing PyGame
'''
pygame.init()

'''
Setting display 
Naming the window
Initializing the clock
'''
gameDisplay = pygame.display.set_mode((800,600))
pygame.display.set_caption("Test::Run")
clock = pygame.time.Clock()

'''
Crash safty, display updating and refreshing setup
'''
crashed = False

while not crashed: 

	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			crashed = True

		print(event)

	pygame.display.update()

	clock.tick(60)

pygame.quit()
quit()